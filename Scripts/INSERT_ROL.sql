/*
Se deben crear 3 registros por medio de un Script en la tabla ROL. Los
registros son los siguientes:
 1,’ADMINISTRADOR’
 2,’AUDITOR’
 3,’AUXILIAR’
*/
INSERT INTO ROL 
(NOMBRE) VALUES ('ADMINISTRADOR'),
('AUDITOR'),
('AUXILIAR');