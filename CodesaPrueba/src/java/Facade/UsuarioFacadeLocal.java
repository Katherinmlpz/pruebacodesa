/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Facade;

import Entity.Usuario;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Katherin Muñoz Lopez
 * @version 1.0
 * @date 01/20/2022
 */
@Local
public interface UsuarioFacadeLocal {

    void create(Usuario usuario);

    void edit(Usuario usuario);

    void remove(Usuario usuario);

    Usuario find(Object id);

    List<Usuario> findAll();

    List<Usuario> findRange(int[] range);
    
    List<String> buscarUsuarios(String busqueda);
    
    Usuario seleccionarUsuario(String busqueda);
    
    List<Usuario> datosUsuarios(String nombreUsuario, int first, int pageSize, String sortField,
            SortOrder sortOrder, Map<String, Object> filters);

    int countUsuarios(String nombreUsuario, int first, int pageSize, String sortField, SortOrder sortOrder,
            Map<String, Object> filters);

    int count();
    
    boolean existeUsuario(String nombre);
    
}
