/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Facade;

import Entity.Usuario;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Katherin Muñoz Lopez
 * @version 1.0
 * @date 01/20/2022
 */
@Stateless
public class UsuarioFacade extends AbstractFacade<Usuario> implements UsuarioFacadeLocal {

    @PersistenceContext(unitName = "CodesaPruebaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioFacade() {
        super(Usuario.class);
    }

    @Override
    public List<String> buscarUsuarios(String busqueda) {
        List<String> lista = new ArrayList();
        try {
            em.getEntityManagerFactory().getCache().evictAll();
            Query q = em.createNativeQuery("select CONCAT(u.id_usuario,' - ',u.nombre,' - ',r.nombre)  "
                    + "FROM  USUARIO u "
                    + "JOIN ROL r ON u.id_rol = r.id_rol "
                    + "WHERE r.nombre like '%" + busqueda + "%'"
                    + " OR u.nombre like '%" + busqueda + "%'"
                    + "	ORDER by u.nombre");
            lista = q.getResultList();
        } catch (Exception e) {

        }
        return lista;
    }
    
    @Override
    public Usuario seleccionarUsuario(String busqueda) {
        Usuario usuario = new Usuario();
        try {
            em.getEntityManagerFactory().getCache().evictAll();
            Query q = em.createQuery("SELECT u "
                    + "FROM  Usuario u "
                    + "WHERE u.idUsuario = " + busqueda + "", Usuario.class);
            
            List<Usuario> usuarios = q.getResultList();
            if (!usuarios.isEmpty()) {
                usuario = usuarios.get(0);
            }else{
                return null;
            }
        } catch (Exception e) {

        }
        return usuario;
    }
    
    public String filtrosBase(Map<String, Object> filters) {
        String salida = "";
        if (filters != null) {
            if (!filters.isEmpty()) {
                for (String string : filters.keySet()) {
                    switch (string) {

                        default:

                            String campo;
                            if (string.contains(".")) {
                                int posicion = string.indexOf(".");
                                campo = string.substring(0, posicion + 1) + string.substring(posicion + 1, posicion + 2).toLowerCase() + string.substring(posicion + 2, string.length());
                                String textValid = campo.substring(posicion + 1, campo.length());
                                if (textValid.contains(".")) {
                                    int posicion2 = textValid.indexOf(".");
                                    String textoBase = textValid.substring(0, posicion2 + 1) + textValid.substring(posicion2 + 1, posicion2 + 2).toLowerCase() + textValid.substring(posicion2 + 2, textValid.length());
                                    campo = campo.substring(0, posicion) + "." + textoBase;
                                    String textValid2 = textValid.substring(posicion2 + 1, textValid.length());
                                    //un nivel mas
                                    if (textValid2.contains(".")) {
                                        int posicion3 = textValid2.indexOf(".");
                                        String textoBase2 = textValid2.substring(0, posicion3 + 1) + textValid2.substring(posicion3 + 1, posicion3 + 2).toLowerCase() + textValid2.substring(posicion3 + 2, textValid2.length());
                                        campo = campo.substring(0, posicion) + "." + textValid.substring(0, posicion2) + "." + textoBase2;
                                    }

                                }

                            } else {
                                campo = string;
                            }

                            salida = salida + " and CAST( c." + campo.substring(0, 1).toLowerCase() + campo.substring(1, campo.length()) + (string.equals("ICondicionPago") ? "*100" : "") + " AS varchar(50) ) like :" + string.replace(".", "") + " ";
                            break;

                    }
                }
                salida = salida.substring(4, salida.length());
            }
        }
        String filtrosSQL = salida.equals("") ? "" : (" AND ( " + salida + " ) ");
        return filtrosSQL;
    }
    
    @Override
    public List<Usuario> datosUsuarios(String nombreUsuario, int first, int pageSize, String sortField, SortOrder sortOrder,
            Map<String, Object> filters) {
        String ordenamiento = "C.nombre DESC";
        List<Usuario> listaUsuarios = new ArrayList<>();
        try {
            if (sortField != null) {
                String string = sortField;
                String campo2;
                if (string.contains(".")) {
                    int posicion = string.indexOf(".");
                    campo2 = string.substring(0, posicion + 1) + string.substring(posicion + 1, posicion + 2).toLowerCase() + string.substring(posicion + 2, string.length());
                    String textValid = campo2.substring(posicion + 1, campo2.length());
                    if (textValid.contains(".")) {
                        int posicion2 = textValid.indexOf(".");
                        String textoBase = textValid.substring(0, posicion2 + 1) + textValid.substring(posicion2 + 1, posicion2 + 2).toLowerCase() + textValid.substring(posicion2 + 2, textValid.length());
                        campo2 = campo2.substring(0, posicion) + "." + textoBase;
                        String textValid2 = textValid.substring(posicion2 + 1, textValid.length());
                        //un nivel mas
                        if (textValid2.contains(".")) {
                            int posicion3 = textValid2.indexOf(".");
                            String textoBase2 = textValid2.substring(0, posicion3 + 1) + textValid2.substring(posicion3 + 1, posicion3 + 2).toLowerCase() + textValid2.substring(posicion3 + 2, textValid2.length());
                            campo2 = campo2.substring(0, posicion) + "." + textValid.substring(0, posicion2) + "." + textoBase2;
                        }

                    }
                } else {
                    campo2 = string;
                }
                ordenamiento = "c." + campo2.substring(0, 1).toLowerCase() + campo2.substring(1, campo2.length()) + (sortOrder.toString().equals("ASCENDING") ? " ASC " : " DESC ");
            }
            String filtrosSQL = filtrosBase(filters);

            Query q = em.createQuery("SELECT C FROM Usuario C"
                    + " WHERE C.nombre like '%" + nombreUsuario + "%' " + filtrosSQL + " ORDER BY " + ordenamiento);
            if (filters != null) {
                filters.keySet().forEach((string) -> {
                    switch (string) {
                        default:
                            q.setParameter(string.replace(".", ""), "%" + filters.get(string) + "%");
                            break;
                    }

                });
            }
            q.setFirstResult(first);
            q.setMaxResults(pageSize);
            listaUsuarios = q.getResultList();
        } catch (Exception e) {
            throw e;
        } finally {
//            em.close();
        }
        return listaUsuarios;
    }
    
    @Override
    public int countUsuarios(String nombreUsuario, int first, int pageSize, String sortField, SortOrder sortOrder,
            Map<String, Object> filters) {
        String ordenamiento = "C.nombre DESC";
        List<Usuario> ListaUsuarios = new ArrayList<>();
        try {
            if (sortField != null) {
                String string = sortField;
                String campo2;
                if (string.contains(".")) {
                    int posicion = string.indexOf(".");
                    campo2 = string.substring(0, posicion + 1) + string.substring(posicion + 1, posicion + 2).toLowerCase() + string.substring(posicion + 2, string.length());
                    String textValid = campo2.substring(posicion + 1, campo2.length());
                    if (textValid.contains(".")) {
                        int posicion2 = textValid.indexOf(".");
                        String textoBase = textValid.substring(0, posicion2 + 1) + textValid.substring(posicion2 + 1, posicion2 + 2).toLowerCase() + textValid.substring(posicion2 + 2, textValid.length());
                        campo2 = campo2.substring(0, posicion) + "." + textoBase;
                        String textValid2 = textValid.substring(posicion2 + 1, textValid.length());
                        //un nivel mas
                        if (textValid2.contains(".")) {
                            int posicion3 = textValid2.indexOf(".");
                            String textoBase2 = textValid2.substring(0, posicion3 + 1) + textValid2.substring(posicion3 + 1, posicion3 + 2).toLowerCase() + textValid2.substring(posicion3 + 2, textValid2.length());
                            campo2 = campo2.substring(0, posicion) + "." + textValid.substring(0, posicion2) + "." + textoBase2;
                        }

                    }
                } else {
                    campo2 = string;
                }
                ordenamiento = "c." + campo2.substring(0, 1).toLowerCase() + campo2.substring(1, campo2.length()) + (sortOrder.toString().equals("ASCENDING") ? " ASC " : " DESC ");
            }
            String filtrosSQL = filtrosBase(filters);

            Query q = em.createQuery("SELECT C FROM Usuario C"
                    + " WHERE C.nombre like '%" + nombreUsuario + "%' " + filtrosSQL + " ORDER BY " + ordenamiento);
            if (filters != null) {
                filters.keySet().forEach((string) -> {
                    switch (string) {
                        default:
                            q.setParameter(string.replace(".", ""), "%" + filters.get(string) + "%");
                            break;
                    }

                });
            }
            ListaUsuarios = q.getResultList();
        } catch (Exception e) {
            throw e;
        } finally {
//            em.close();
        }
        return ListaUsuarios.size();
    }
    
    @Override
    public boolean existeUsuario(String nombre){
        List<String> lista = new ArrayList();
        try {
            em.getEntityManagerFactory().getCache().evictAll();
            Query q = em.createNativeQuery("select *  "
                    + "FROM  USUARIO u "
                    + "WHERE u.nombre = '" + nombre + "'");
            lista = q.getResultList();
            if(!lista.isEmpty()){
                return true;
            }
        } catch (Exception e) {

        }
        return false;
    }

}
