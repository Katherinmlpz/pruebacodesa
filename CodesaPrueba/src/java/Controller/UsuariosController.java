/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Entity.Rol;
import Entity.Usuario;
import Facade.RolFacadeLocal;
import Facade.UsuarioFacadeLocal;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Katherin Muñoz Lopez
 * @version 1.0
 * @date 01/20/2022
 */
@Named(value = "usuariosController")
@ViewScoped
public class UsuariosController implements Serializable {

    //Inject facades
    @Inject
    RolFacadeLocal rolFacadeLocal;
    @Inject
    UsuarioFacadeLocal usuarioFacadeLocal;
    //Entidades
    private Rol rolEntity;
    private Usuario usuarioEntity;
    //Variables
    private String busquedaUsuario;
    private LazyDataModel<Usuario> listaUsuarios;
    private List<Usuario> listaU;
    private Usuario selectedUsuario;
    private Usuario crearNuevo;
    private Boolean editar;
    private String header;
    //Filtros
    private String nombreUsuarioFiltro;
    private int regTotalUsuarios;
    private SortOrder sortOrderUsuario;
    private String sortFieldUsuario;
    private Map<String, Object> filtersUsuario;
    //Nuevo Usuario
    private String nuevoUsuarioNombre = "";
    private int nuevoUsuarioRol = 0;
    private String nuevoUsuarioActivo = "";

    public RolFacadeLocal getRolFacadeLocal() {
        return rolFacadeLocal;
    }

    public void setRolFacadeLocal(RolFacadeLocal rolFacadeLocal) {
        this.rolFacadeLocal = rolFacadeLocal;
    }

    public UsuarioFacadeLocal getUsuarioFacadeLocal() {
        return usuarioFacadeLocal;
    }

    public void setUsuarioFacadeLocal(UsuarioFacadeLocal usuarioFacadeLocal) {
        this.usuarioFacadeLocal = usuarioFacadeLocal;
    }

    public Rol getRolEntity() {
        return rolEntity;
    }

    public void setRolEntity(Rol rolEntity) {
        this.rolEntity = rolEntity;
    }

    public Usuario getUsuarioEntity() {
        return usuarioEntity;
    }

    public void setUsuarioEntity(Usuario usuarioEntity) {
        this.usuarioEntity = usuarioEntity;
    }

    public String getBusquedaUsuario() {
        return busquedaUsuario;
    }

    public void setBusquedaUsuario(String busquedaUsuario) {
        this.busquedaUsuario = busquedaUsuario;
    }

    public LazyDataModel<Usuario> getListaUsuarios() {
        return listaUsuarios;
    }

    public void setListaUsuarios(LazyDataModel<Usuario> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }

    public List<Usuario> getListaU() {
        return listaU;
    }

    public void setListaU(List<Usuario> listaU) {
        this.listaU = listaU;
    }

    public String getNombreUsuarioFiltro() {
        return nombreUsuarioFiltro;
    }

    public void setNombreUsuarioFiltro(String nombreUsuarioFiltro) {
        this.nombreUsuarioFiltro = nombreUsuarioFiltro;
    }

    public int getRegTotalUsuarios() {
        return regTotalUsuarios;
    }

    public void setRegTotalUsuarios(int regTotalUsuarios) {
        this.regTotalUsuarios = regTotalUsuarios;
    }

    public SortOrder getSortOrderUsuario() {
        return sortOrderUsuario;
    }

    public void setSortOrderUsuario(SortOrder sortOrderUsuario) {
        this.sortOrderUsuario = sortOrderUsuario;
    }

    public String getSortFieldUsuario() {
        return sortFieldUsuario;
    }

    public void setSortFieldUsuario(String sortFieldUsuario) {
        this.sortFieldUsuario = sortFieldUsuario;
    }

    public Map<String, Object> getFiltersUsuario() {
        return filtersUsuario;
    }

    public void setFiltersUsuario(Map<String, Object> filtersUsuario) {
        this.filtersUsuario = filtersUsuario;
    }

    public Usuario getSelectedUsuario() {
        return selectedUsuario;
    }

    public void setSelectedUsuario(Usuario selectedUsuario) {
        this.selectedUsuario = selectedUsuario;
    }

    public Boolean getEditar() {
        return editar;
    }

    public void setEditar(Boolean editar) {
        this.editar = editar;
    }

    public Usuario getCrearNuevo() {
        return crearNuevo;
    }

    public void setCrearNuevo(Usuario crearNuevo) {
        this.crearNuevo = crearNuevo;
    }

    public String getNuevoUsuarioNombre() {
        return nuevoUsuarioNombre;
    }

    public void setNuevoUsuarioNombre(String nuevoUsuarioNombre) {
        this.nuevoUsuarioNombre = nuevoUsuarioNombre;
    }

    public int getNuevoUsuarioRol() {
        return nuevoUsuarioRol;
    }

    public void setNuevoUsuarioRol(int nuevoUsuarioRol) {
        this.nuevoUsuarioRol = nuevoUsuarioRol;
    }

    public String getNuevoUsuarioActivo() {
        return nuevoUsuarioActivo;
    }

    public void setNuevoUsuarioActivo(String nuevoUsuarioActivo) {
        this.nuevoUsuarioActivo = nuevoUsuarioActivo;
    }

    public String getHeader() {
        return header;
    }
    
    public UsuariosController() {
    }

    @PostConstruct
    public void init() {
        this.rolEntity = new Rol();
        this.usuarioEntity = new Usuario();

        this.busquedaUsuario = "";
        this.listaUsuarios = null;

        this.nombreUsuarioFiltro = "";
        this.listaU = null;

        this.editar = false;

        this.crearNuevo = new Usuario();
        this.crearNuevo.setIdRol(new Rol());

        this.nuevoUsuarioNombre = "";
        this.nuevoUsuarioRol = 1;
        this.nuevoUsuarioActivo = "";
    }

    //METODOS
    public List<String> buscarUsuarios(String busqueda) {
        this.nombreUsuarioFiltro = busqueda;
        this.generarDataTable();
        RequestContext.getCurrentInstance().update("main:creacion");
        return usuarioFacadeLocal.buscarUsuarios(busqueda);
    }

    public void seleccionUsuario() {
        try {
            this.setUsuarioEntity(this.usuarioFacadeLocal.seleccionarUsuario(this.getBusquedaUsuario().split("-")[0].trim()));
            this.setNombreUsuarioFiltro(this.usuarioEntity.getNombre());
            this.generarDataTable();
        } catch (Exception e) {
        }
    }

    public void generarDataTable() {
        try {
            listaUsuarios = new LazyDataModel<Usuario>() {
                @Override
                public List<Usuario> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                    listaU = usuarioFacadeLocal.datosUsuarios(nombreUsuarioFiltro, first, pageSize, sortField, sortOrder, filters);

                    regTotalUsuarios = usuarioFacadeLocal.countUsuarios(nombreUsuarioFiltro, first, pageSize, sortField, sortOrder, filters);
                    sortOrderUsuario = sortOrder;
                    sortFieldUsuario = sortField;
                    filtersUsuario = filters;
                    setRowCount(regTotalUsuarios);
                    RequestContext.getCurrentInstance().execute("actualizarTotalU();");
                    return listaU;
                }

                @Override
                public Usuario getRowData(String rowKey) {

                    for (Usuario usuario : listaUsuarios) {
                        if (usuario.getIdUsuario().toString().equals(rowKey)) {
                            return usuario;
                        }
                    }
                    return null;
                }

                @Override
                public Object getRowKey(Usuario usuario) {
                    return usuario;
                }
            };
        } catch (Exception e) {

        }
    }

    public void selectUsuarioTable() {
        this.editar = false;
        RequestContext.getCurrentInstance().update("main:productDetail");
        RequestContext.getCurrentInstance().execute("PF('productDialog').show()");
    }

    public void crearUsuario() {
        RequestContext.getCurrentInstance().update("main:panelAdmin");
        RequestContext.getCurrentInstance().execute("PF('panelAdmin').show()");
    }

    public List<Rol> getRol() {
        return rolFacadeLocal.findAll();
    }

    public void accionUsuario(int accion) throws IOException {
        switch (accion) {
            case 1:
                try {
                    if (this.usuarioFacadeLocal.existeUsuario(this.selectedUsuario.getNombre())) {
                        RequestContext.getCurrentInstance().execute("swal({ "
                                + "  title: \"Usuario ya existe!\", "
                                + "  text: \"Ya se encuentra un usuario con este nombre creado !!!!\", "
                                + "  type: \"warning\", "
                                + "  button: \"OK\", "
                                + "});");
                    }
                    this.usuarioFacadeLocal.edit(this.selectedUsuario);
                    RequestContext.getCurrentInstance().execute("swal({ "
                            + "  title: \"Se ha actualizado el usuario con éxito!\", "
                            + "  text: \"Usuario actualizado de forma exitosa !!!!\", "
                            + "  type: \"success\", "
                            + "  button: \"OK\", "
                            + "});");
                } catch (Exception e) {
                }
                break;
            case 2:
                try {
                    this.usuarioFacadeLocal.remove(this.selectedUsuario);
                    RequestContext.getCurrentInstance().execute("swal({ "
                            + "  title: \"Se ha eliminado el usuario con éxito!\", "
                            + "  text: \"Usuario eliminado de forma exitosa !!!!\", "
                            + "  type: \"success\", "
                            + "  button: \"OK\", "
                            + "});");
                } catch (Exception e) {
                }
                break;
            case 3:
                try {
                    if (this.usuarioFacadeLocal.existeUsuario(nuevoUsuarioNombre)) {
                        RequestContext.getCurrentInstance().execute("swal({ "
                                + "  title: \"Usuario ya existe!\", "
                                + "  text: \"Ya se encuentra un usuario con este nombre creado !!!!\", "
                                + "  type: \"warning\", "
                                + "  button: \"OK\", "
                                + "});");
                    } else {

                        if (nuevoUsuarioNombre.equals("")) {
                            RequestContext.getCurrentInstance().execute("warn('Debe especificar un nombre de usuario. ')");
                        }
                        if (nuevoUsuarioActivo.equals("")) {
                            RequestContext.getCurrentInstance().execute("warn('Debe especificar el estado Activo para el usuario. ')");
                        }
                        Usuario nuevo = new Usuario();
                        nuevo.setIdRol(new Rol(nuevoUsuarioRol));
                        nuevo.setNombre(nuevoUsuarioNombre);
                        nuevo.setActivo(nuevoUsuarioActivo.charAt(0));
                        this.usuarioFacadeLocal.create(nuevo);
                        RequestContext.getCurrentInstance().execute("swal({ "
                                + "  title: \"Se ha creado el usuario con éxito!\", "
                                + "  text: \"Usuario creado de forma exitosa !!!!\", "
                                + "  type: \"success\", "
                                + "  button: \"OK\", "
                                + "});");
                    }

                } catch (Exception e) {
                }
                break;
            case 4:
                this.editar = true;
                RequestContext.getCurrentInstance().update("main:productDetail");
                RequestContext.getCurrentInstance().execute("PF('productDialog').show()");
                break;
            case 5:
                this.nombreUsuarioFiltro = "";
                this.generarDataTable();
                break;
            case 6:
                this.init();
                break;
        }
    }
}
