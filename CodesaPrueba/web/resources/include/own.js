/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var unidades = 0;
var decenas = 0;
var centenas = 0;
var unidadesMillar = 0;
var listaMuestra = 0;
var secUnidades = 0;
var secDecenas = 0;
var secCentenas = 0;
var secUnidadesMillar = 0;
var tipoSorteo = 0;
var flacher = 0;
var digitoActual = 1;
var cantidadVeces = 200;
var color = "";
var ganador = 0;
var oAudioMission = document.getElementById("miAudioMission");
var oAudioAplausos = document.getElementById("miAudioAplausos");
var oAudioJb = document.getElementById("miGente");

var listaGanadores = [];

function valida(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    //Tecla de retroceso para borrar, siempre la permite
    if (tecla == 8) {
        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros
    patron = /[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}

function validaAlfanumerico(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    //Tecla de retroceso para borrar, siempre la permite
    if (tecla == 8) {
        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros
    patron = /[A-Za-z0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}


function start() {
    $('#temporizador').modal('show');
}

function stop() {
    $('#temporizador').modal('hide');
}


function clicBotones(formulario, idboton) {
    var botonString = "#" + formulario + "\\:" + idboton;
    alert(botonString);
    $(botonString).click();
}


function colorPanelCotizador(valor) {
    document.getElementById('miPanel1').classList.remove("bg-gray-active");
    document.getElementById('miPanel2').classList.remove("bg-gray-active");
    document.getElementById('miPanel3').classList.remove("bg-gray-active");
    document.getElementById('miPanel4').classList.remove("bg-gray-active");
    document.getElementById('miPanel5').classList.remove("bg-gray-active");
    document.getElementById('miPanel6').classList.remove("bg-gray-active");
    switch (valor) {
        case 1:
            document.getElementById('miPanel1').classList.add("bg-gray-active");
            document.getElementById('miPanel11').classList.add("bg-gray-active");
            break;
        case 2:
            document.getElementById('miPanel2').classList.add("bg-gray-active");
            document.getElementById('miPanel12').classList.add("bg-gray-active");
            break;
        case 3:
            document.getElementById('miPanel3').classList.add("bg-gray-active");
            document.getElementById('miPanel13').classList.add("bg-gray-active");
            break;
        case 4:
            document.getElementById('miPanel4').classList.add("bg-gray-active");
            document.getElementById('miPanel14').classList.add("bg-gray-active");
            break;
        case 5:
            document.getElementById('miPanel5').classList.add("bg-gray-active");
            document.getElementById('miPanel15').classList.add("bg-gray-active");
            break;
        case 6:
            document.getElementById('miPanel6').classList.add("bg-gray-active");
            document.getElementById('miPanel16').classList.add("bg-gray-active");
            break;
        default:

    }


}


function colorPanelVisitador(valor) {
    document.getElementById('miPanel1').classList.remove("bg-gray-active");
    document.getElementById('miPanel2').classList.remove("bg-gray-active");
    document.getElementById('miPanel3').classList.remove("bg-gray-active");
    document.getElementById('miPanel11').classList.remove("bg-gray-active");
    document.getElementById('miPanel22').classList.remove("bg-gray-active");
    document.getElementById('miPanel33').classList.remove("bg-gray-active");
    switch (valor) {
        case 1:
            document.getElementById('miPanel1').classList.add("bg-gray-active");
            document.getElementById('miPanel11').classList.add("bg-gray-active");
            break;
        case 2:
            document.getElementById('miPanel2').classList.add("bg-gray-active");
            document.getElementById('miPanel22').classList.add("bg-gray-active");
            break;
        case 3:
            document.getElementById('miPanel3').classList.add("bg-gray-active");
            document.getElementById('miPanel33').classList.add("bg-gray-active");
            break;
        default:

    }

}




function colorPanel(valor) {
    document.getElementById('miPanel1').classList.remove("bg-gray-active");
    document.getElementById('miPanel2').classList.remove("bg-gray-active");
    document.getElementById('miPanel3').classList.remove("bg-gray-active");
    document.getElementById('miPanel11').classList.remove("bg-gray-active");
    document.getElementById('miPanel22').classList.remove("bg-gray-active");
    document.getElementById('miPanel33').classList.remove("bg-gray-active");
    document.getElementById('miPanel4').classList.remove("bg-gray-active");
    document.getElementById('miPanel5').classList.remove("bg-gray-active");
    document.getElementById('miPanel6').classList.remove("bg-gray-active");
    switch (valor) {
        case 1:
            document.getElementById('miPanel1').classList.add("bg-gray-active");
            document.getElementById('miPanel11').classList.add("bg-gray-active");
            break;
        case 2:
            document.getElementById('miPanel2').classList.add("bg-gray-active");
            document.getElementById('miPanel22').classList.add("bg-gray-active");
            break;
        case 3:
            document.getElementById('miPanel3').classList.add("bg-gray-active");
            document.getElementById('miPanel33').classList.add("bg-gray-active");
            break;
        case 4:
            document.getElementById('miPanel4').classList.add("bg-gray-active");
            break;
        case 5:
            document.getElementById('miPanel5').classList.add("bg-gray-active");
            break;
        case 6:
            document.getElementById('miPanel6').classList.add("bg-gray-active");
            break;
        default:

    }


}

function colorPanelGe(valor) {
    document.getElementById('miPanel1').classList.remove("bg-gray-active");
    document.getElementById('miPanel2').classList.remove("bg-gray-active");
    document.getElementById('miPanel3').classList.remove("bg-gray-active");
    document.getElementById('miPanel4').classList.remove("bg-gray-active");
    document.getElementById('miPanel5').classList.remove("bg-gray-active");
    document.getElementById('miPanel6').classList.remove("bg-gray-active");
    document.getElementById('miPanel11').classList.remove("bg-gray-active");
    document.getElementById('miPanel22').classList.remove("bg-gray-active");
    document.getElementById('miPanel33').classList.remove("bg-gray-active");
    document.getElementById('miPanel44').classList.remove("bg-gray-active");
    document.getElementById('miPanel55').classList.remove("bg-gray-active");
    document.getElementById('miPanel66').classList.remove("bg-gray-active");
    document.getElementById('miPanel8').classList.remove("bg-gray-active");
    document.getElementById('miPanel8').classList.add("bg-green-active");
    document.getElementById('miPanel9').classList.remove("bg-gray-active");
    document.getElementById('miPanel10').classList.remove("bg-gray-active");
    document.getElementById('miPanel111').classList.remove("bg-gray-active");
    switch (valor) {
        case 1:
            document.getElementById('miPanel1').classList.add("bg-gray-active");
            document.getElementById('miPanel11').classList.add("bg-gray-active");
            break;
        case 2:
            document.getElementById('miPanel2').classList.add("bg-gray-active");
            document.getElementById('miPanel22').classList.add("bg-gray-active");
            break;
        case 3:
            document.getElementById('miPanel3').classList.add("bg-gray-active");
            document.getElementById('miPanel33').classList.add("bg-gray-active");
            break;
        case 4:
            document.getElementById('miPanel4').classList.add("bg-gray-active");
            document.getElementById('miPanel44').classList.add("bg-gray-active");
            break;
        case 5:
            document.getElementById('miPanel5').classList.add("bg-gray-active");
            document.getElementById('miPanel55').classList.add("bg-gray-active");
            break;
        case 6:
            document.getElementById('miPanel6').classList.add("bg-gray-active");
            document.getElementById('miPanel66').classList.add("bg-gray-active");
            break;
        case 7:
            document.getElementById('miPanel7').classList.add("bg-gray-active");
            break;
        case 8:
            document.getElementById('miPanel8').classList.remove("bg-green-active");
            document.getElementById('miPanel8').classList.add("bg-gray-active");
            break;
        case 9:
            document.getElementById('miPanel9').classList.add("bg-gray-active");
            break;
        case 10:
            document.getElementById('miPanel10').classList.add("bg-gray-active");
            break;
        case 111:
            document.getElementById('miPanel111').classList.add("bg-gray-active");
            break;
        case 12:
            document.getElementById('miPanel12').classList.remove("bg-green-active");
            document.getElementById('miPanel12').classList.add("bg-gray-active");
            break;
        default:
            break;
    }


}



function inicio(tSorteo) {
    listaGanadores = JSON.parse(localStorage.getItem("listaGanadores"));
    // alert(Array.isArray(listaGanadores));
    tipoSorteo = tSorteo;
    if (tipoSorteo == 2) {
        listaMuestra = 4;
        flacher = 0;
    }
    secUnidades = 0;
    secDecenas = 0;
    secCentenas = 0;
    secUnidadesMillar = 0;
    color = "";
    ganador = 0;
    oAudioMission.play();

    if (Array.isArray(listaGanadores)) {
        var aleatorio = Math.floor(Math.random() * document.getElementById("invisible:empleados").value) + 1;
        var pos = 0;
        for (var i = 0; i < listaGanadores.length; i++) {
            if (aleatorio === listaGanadores[i]) {
                i = 0;
                aleatorio = Math.floor(Math.random() * document.getElementById("invisible:empleados").value) + 1;
            }
            pos++;
        }
        listaGanadores[listaGanadores.length] = aleatorio;
        ganador = aleatorio;
    } else {
        listaGanadores = [];
        listaGanadores[0] = Math.floor(Math.random() * document.getElementById("invisible:empleados").value) + 1;
        ganador = listaGanadores[0];
    }

    localStorage['listaGanadores'] = JSON.stringify(listaGanadores);
//    for (var i = 0; i < listaGanadores.length; i++) {
//
//        alert("pos : " + i + " valor : " + listaGanadores[i]);
//    }

    var temp = "" + ganador;
    var n = temp.length;
    //  document.getElementById("ventana").innerHTML = document.getElementById("invisible:numeroGanadores").value;
    // document.getElementById("ventana").innerHTML = tSorteo;


    switch (n) {
        case 1:
            secUnidades = temp;
            secDecenas = 0;
            secCentenas = 0;
            secUnidadesMillar = 0;
            break;
        case 2:
            secUnidades = temp.substring(1, 2);
            secDecenas = temp.substring(0, 1);
            secCentenas = 0;
            secUnidadesMillar = 0;
            break;
        case 3:
            secUnidades = temp.substring(2, 3);
            secDecenas = temp.substring(1, 2);
            secCentenas = temp.substring(0, 1);
            secUnidadesMillar = 0;
            break;
        default:
            secUnidades = temp.substring(3, 4);
            secDecenas = temp.substring(2, 3);
            secCentenas = temp.substring(1, 2);
            secUnidadesMillar = temp.substring(0, 1);
    }


    document.getElementById("dig1").innerHTML = 0;
    document.getElementById("dig2").innerHTML = 0;
    document.getElementById("dig3").innerHTML = 0;
    document.getElementById("dig4").innerHTML = 0;
    //  document.getElementById("ganador").innerHTML = ganador;
    document.getElementById("jugar").disabled = true;
    var nGanadores = document.getElementById("invisible:numeroGanadores").value;
    if (nGanadores > 0) {


        if (digitoActual == 5) {
            oAudioAplausos.pause();
            if (cantidadVeces == 0) {

                document.getElementById("dig4").classList.remove("red");
                document.getElementById("dig4").classList.add("blue");
                document.getElementById("dig3").classList.remove("red");
                document.getElementById("dig3").classList.add("blue");
                document.getElementById("dig2").classList.remove("red");
                document.getElementById("dig2").classList.add("blue");
                document.getElementById("dig1").classList.remove("red");
                document.getElementById("dig1").classList.add("blue");
            } else {
                document.getElementById("dig4").classList.remove("blue");
                document.getElementById("dig4").classList.add("red");
                document.getElementById("dig3").classList.remove("blue");
                document.getElementById("dig3").classList.add("red");
                document.getElementById("dig2").classList.remove("blue");
                document.getElementById("dig2").classList.add("red");
                document.getElementById("dig1").classList.remove("blue");
                document.getElementById("dig1").classList.add("red");
            }

        }
        cantidadVeces = 200;
        digitoActual = 1;
        clearInterval(control);
    }
    control = setInterval(cronometro, 20);
}



function retornaColor(unid, tipo) {
    if (tipo == 1) {
        if (unid == 0) {
            unid = 9;
        } else {
            unid--;
        }
    }
    switch (unid) {
        case 1:
            color = "red";
            break;
        case 2:
            color = "grey";
            break;
        case 3:
            color = "green";
            break;
        case 4:
            color = "lightgreen";
            break;
        case 5:
            color = "yellow";
            break;
        case 6:
            color = "orange";
            break;
        case 7:
            color = "purple";
            break;
        case 8:
            color = "silver";
            break;
        case 9:
            color = "salmon";
            break;
        default:
            color = "blue";
    }
    return color;
}

function inicioCedula() {
    document.getElementById("invisible:Jugar").disabled = true;
    ganador = document.getElementById("invisible:oculto").value;
    digitoActual = 1;
    cantidadVeces = 200 - digitoActual * 10;
    secUnidades = 0;
    secDecenas = ganador.length;
    secDecenas++;
    flacher = 50;
    oAudioJb.play();
    controlCedula = setInterval(cronometroCedula, flacher);
}

function cronometroCedula() {
//    alert(digitoActual);

    if (cantidadVeces > 0) {
        if (secUnidades <= 9) {
            secUnidades++;
            document.getElementById("dig" + digitoActual).innerHTML = secUnidades;
        }
        if (secUnidades == 10) {
            secUnidades = 0;
            document.getElementById("dig" + digitoActual).innerHTML = 0;
        }
        document.getElementById("dig" + digitoActual).classList.remove(retornaColor(secUnidades, 1));
        document.getElementById("dig" + digitoActual).classList.add(retornaColor(secUnidades, 2));
        cantidadVeces--;
    } else {
        document.getElementById("dig" + digitoActual).classList.remove(retornaColor(secUnidades, 2));
        document.getElementById("dig" + digitoActual).classList.add("red");
        var ini = digitoActual - 1;
        var fin = digitoActual;
        document.getElementById("dig" + digitoActual).innerHTML = ganador.substring(ini, fin);
        digitoActual++;
        cantidadVeces = 200 - digitoActual * 10;
        flacher = flacher - 4;
        clearInterval(controlCedula);
        controlCedula = setInterval(cronometroCedula, flacher);
    }
    if (digitoActual === secDecenas) {
        clearInterval(controlCedula);
        controlDestellar = setInterval(destallar, 600);
        cantidadVeces = 0;
        unidades = 0;
        oAudioJb.pause();
        oAudioAplausos.play();
        muestraGanador();
    }
}


function destallar() {

    if (unidades === 20) {
        document.getElementById("invisible:Jugar").disabled = false;
        clearInterval(controlDestellar);
        oAudioAplausos.pause();
    } else {
        if (cantidadVeces == 0) {
            for (var i = 1, max = secDecenas; i < max; i++) {
                document.getElementById("dig" + i).classList.remove("red");
                document.getElementById("dig" + i).classList.add("blue");
            }
            cantidadVeces = 1;
        } else {
            for (var i = 1, max = secDecenas; i < max; i++) {
                document.getElementById("dig" + i).classList.remove("blue");
                document.getElementById("dig" + i).classList.add("red");
            }
            cantidadVeces = 0;
        }
    }
    unidades++;
}




function cronometro() {

    if (digitoActual == 1) {

        if (cantidadVeces > 0) {
            if (secUnidadesMillar <= 9) {
                secUnidadesMillar++;
                dig1.innerHTML = secUnidadesMillar;
            }
            if (secUnidadesMillar == 10) {
                secUnidadesMillar = 0;
                dig1.innerHTML = 0;
            }
            document.getElementById("dig1").classList.remove(retornaColor(secUnidadesMillar, 1));
            document.getElementById("dig1").classList.add(retornaColor(secUnidadesMillar, 2));
            cantidadVeces--;
        } else {
            document.getElementById("dig1").classList.remove(retornaColor(secUnidadesMillar, 2));
            document.getElementById("dig1").classList.add("red");
            cantidadVeces = 150;
            digitoActual = 2;
            clearInterval(control);
            control = setInterval(cronometro, 40);
        }

    }


    if (digitoActual == 2) {

        if (cantidadVeces > 0) {
            if (secCentenas <= 9) {
                secCentenas++;
                dig2.innerHTML = secCentenas;
            }
            if (secCentenas == 10) {
                secCentenas = 0;
                dig2.innerHTML = 0;
            }
            document.getElementById("dig2").classList.remove(retornaColor(secCentenas, 1));
            document.getElementById("dig2").classList.add(retornaColor(secCentenas, 2));
            cantidadVeces--;
        } else {
            document.getElementById("dig2").classList.remove(retornaColor(secCentenas, 2));
            document.getElementById("dig2").classList.add("red");
            cantidadVeces = 100;
            digitoActual = 3;
            clearInterval(control);
            control = setInterval(cronometro, 50);
        }


    }

    if (digitoActual == 3) {

        if (cantidadVeces > 0) {
            if (secDecenas <= 9) {
                secDecenas++;
                dig3.innerHTML = secDecenas;
            }
            if (secDecenas == 10) {
                secDecenas = 0;
                dig3.innerHTML = 0;
            }
            document.getElementById("dig3").classList.remove(retornaColor(secDecenas, 1));
            document.getElementById("dig3").classList.add(retornaColor(secDecenas, 2));
            cantidadVeces--;
        } else {
            document.getElementById("dig3").classList.remove(retornaColor(secDecenas, 2));
            document.getElementById("dig3").classList.add("red");
            cantidadVeces = 50;
            digitoActual = 4;
            clearInterval(control);
            control = setInterval(cronometro, 60);
        }


    }

    if (digitoActual == 4) {

        if (cantidadVeces > 0) {
            if (secUnidades <= 9) {
                secUnidades++;
                dig4.innerHTML = secUnidades;
            }
            if (secUnidades == 10) {
                secUnidades = 0;
                dig4.innerHTML = 0;
            }
            document.getElementById("dig4").classList.remove(retornaColor(secUnidades, 1));
            document.getElementById("dig4").classList.add(retornaColor(secUnidades, 2));
            cantidadVeces--;
        } else {
            document.getElementById("dig4").classList.remove(retornaColor(secUnidades, 2));
            document.getElementById("dig4").classList.add("red");
            digitoActual = 5;
            cantidadVeces = 0;
            clearInterval(control);
            control = setInterval(cronometro, 1000);
            if (tipoSorteo == 1) {
                document.getElementById("jugar").disabled = false;
            }
            document.getElementById("invisible:oculto").value = "" + ganador;
            $("#invisible\\:panel").click();
            if (tipoSorteo == 1) {
                oAudioMission.pause();
                oAudioAplausos.play();
            }

        }

    }

    if (digitoActual == 5) {



        if (cantidadVeces == 0) {

            document.getElementById("dig4").classList.remove("red");
            document.getElementById("dig4").classList.add("blue");
            document.getElementById("dig3").classList.remove("red");
            document.getElementById("dig3").classList.add("blue");
            document.getElementById("dig2").classList.remove("red");
            document.getElementById("dig2").classList.add("blue");
            document.getElementById("dig1").classList.remove("red");
            document.getElementById("dig1").classList.add("blue");
            cantidadVeces = 1;
        } else {

            document.getElementById("dig4").classList.remove("blue");
            document.getElementById("dig4").classList.add("red");
            document.getElementById("dig3").classList.remove("blue");
            document.getElementById("dig3").classList.add("red");
            document.getElementById("dig2").classList.remove("blue");
            document.getElementById("dig2").classList.add("red");
            document.getElementById("dig1").classList.remove("blue");
            document.getElementById("dig1").classList.add("red");
            cantidadVeces = 0;
        }

        if (tipoSorteo == 2) {
            flacher++;
            if (flacher == 9) {

                if (listaMuestra > 0) {

                    secUnidades = 0;
                    secDecenas = 0;
                    secCentenas = 0;
                    secUnidadesMillar = 0;
                    color = "";
                    ganador = 0;
                    flacher = 0;
                    listaGanadores = JSON.parse(localStorage.getItem("listaGanadores"));
                    if (Array.isArray(listaGanadores)) {
                        var aleatorio = Math.floor(Math.random() * document.getElementById("invisible:empleados").value) + 1;
                        var pos = 0;
                        for (var i = 0; i < listaGanadores.length; i++) {
                            if (aleatorio === listaGanadores[i]) {
                                i = 0;
                                aleatorio = Math.floor(Math.random() * document.getElementById("invisible:empleados").value) + 1;
                            }
                            pos++;
                        }
                        listaGanadores[listaGanadores.length] = aleatorio;
                        ganador = aleatorio;
                    } else {
                        listaGanadores = [];
                        listaGanadores[0] = Math.floor(Math.random() * document.getElementById("invisible:empleados").value) + 1;
                        ganador = listaGanadores[0];
                    }
                    localStorage['listaGanadores'] = JSON.stringify(listaGanadores);
                    //    alert(listaGanadores);
                    var temp = "" + ganador;
                    var n = temp.length;
                    document.getElementById("dig1").innerHTML = 0;
                    document.getElementById("dig2").innerHTML = 0;
                    document.getElementById("dig3").innerHTML = 0;
                    document.getElementById("dig4").innerHTML = 0;
                    switch (n) {
                        case 1:
                            secUnidades = temp;
                            secDecenas = 0;
                            secCentenas = 0;
                            secUnidadesMillar = 0;
                            break;
                        case 2:
                            secUnidades = temp.substring(1, 2);
                            secDecenas = temp.substring(0, 1);
                            secCentenas = 0;
                            secUnidadesMillar = 0;
                            break;
                        case 3:
                            secUnidades = temp.substring(2, 3);
                            secDecenas = temp.substring(1, 2);
                            secCentenas = temp.substring(0, 1);
                            secUnidadesMillar = 0;
                            break;
                        default:
                            secUnidades = temp.substring(3, 4);
                            secDecenas = temp.substring(2, 3);
                            secCentenas = temp.substring(1, 2);
                            secUnidadesMillar = temp.substring(0, 1);
                    }
                    cantidadVeces = 200;
                    digitoActual = 1;
                    clearInterval(control);
                    control = setInterval(cronometro, 20);
                    listaMuestra--;
                } else {
                    document.getElementById("jugar").disabled = false;
                    oAudioMission.pause();
                    oAudioAplausos.play();
                }

            }

        }

    }
}

function verificarCaja() {
    var caja = document.getElementById("consultaPanel1:inputConsulta").value;
    $('#consultaPanel1\\:btnConsulta').attr('disabled', true);
    if (caja.length > 3) {
        $('#consultaPanel1\\:btnConsulta').attr('disabled', false);
    }

}


$(function () {
    $('.btn-circle').on('click', function () {
        $('.btn-circle.btn-info').removeClass('btn-info').addClass('btn-default');
        $(this).addClass('btn-info').removeClass('btn-default').blur();
    });
    $('.next-step, .prev-step').on('click', function (e) {
        var $activeTab = $('.tab-pane.active');
        $('.btn-circle.btn-info').removeClass('btn-info').addClass('btn-default');
        if ($(e.target).hasClass('next-step'))
        {
            var nextTab = $activeTab.next('.tab-pane').attr('id');
            $('[href="#' + nextTab + '"]').addClass('btn-info').removeClass('btn-default');
            $('[href="#' + nextTab + '"]').tab('show');
        } else
        {
            var prevTab = $activeTab.prev('.tab-pane').attr('id');
            $('[href="#' + prevTab + '"]').addClass('btn-info').removeClass('btn-default');
            $('[href="#' + prevTab + '"]').tab('show');
        }
    });
});


function actualizar(nombre) {

    var tiempo = new Date();
    var hora, cad = nombre;
    with (tiempo) {
        hora = getHours();
//        cad += hora + ":" + getMinutes() + ":" + getSeconds();
    }

    if (new Date().getHours() < 12)
        cad = "Buenos días, ";
    else if (new Date().getHours() < 18)
        cad = "Buenas tardes, " + cad;
    else
        cad = "Buenas noches, " + cad;
    if (Notification.permission !== "granted")
        Notification.requestPermission();
    else {
        var notification = new Notification(cad, {
            icon: 'https://sic.casalimpia.co/Sic/images/version2.png',
            body: "Si tiene dificultades no dude de comunicarse con nuestro TEAM Desarrollo : 4578383 Ext 8407 !!!",
        });
//        notification.onclick = function () {
//            window.open("http://stackoverflow.com/a/13328397/1269037");
//        };

    }
}

function saludo(nombre) {

    var tiempo = new Date();
    var hora, cad = nombre;
    with (tiempo) {
        hora = getHours();
//        cad += hora + ":" + getMinutes() + ":" + getSeconds();
    }

    if (new Date().getHours() < 12)
        cad = "Buenos días, ";
    else if (new Date().getHours() < 18)
        cad = "Buenas tardes, " + cad;
    else
        cad = "Buenas noches, " + cad;
    if (Notification.permission !== "granted")
        Notification.requestPermission();
    else {
        var notification = new Notification(cad, {
            icon: 'https://sic.casalimpia.co/Sic/images/version2.png',
            body: "No olvide mantener los datos actualizados es muy importante para nosotros. !!!",
        });
//        notification.onclick = function () {
//            window.open("http://stackoverflow.com/a/13328397/1269037");
//        };

    }
}

function colorPanelTigo(valor) {
    document.getElementById('miPanel1').classList.remove("bg-gray-active");
    document.getElementById('miPanel2').classList.remove("bg-gray-active");
    document.getElementById('miPanel3').classList.remove("bg-gray-active");
    document.getElementById('miPanel4').classList.remove("bg-gray-active");
    document.getElementById('miPanel5').classList.remove("bg-gray-active");
    document.getElementById('miPanel6').classList.remove("bg-gray-active");
    document.getElementById('miPanel7').classList.remove("bg-gray-active");
    document.getElementById('miPanel8').classList.remove("bg-gray-active");
    document.getElementById('miPanel11').classList.remove("bg-gray-active");
    document.getElementById('miPanel22').classList.remove("bg-gray-active");
    document.getElementById('miPanel33').classList.remove("bg-gray-active");
    document.getElementById('miPanel44').classList.remove("bg-gray-active");
    document.getElementById('miPanel55').classList.remove("bg-gray-active");
    document.getElementById('miPanel66').classList.remove("bg-gray-active");
    document.getElementById('miPanel77').classList.remove("bg-gray-active");
    document.getElementById('miPanel88').classList.remove("bg-gray-active");
    switch (valor) {
        case 1:
            document.getElementById('miPanel1').classList.add("bg-gray-active");
            document.getElementById('miPanel11').classList.add("bg-gray-active");
            break;
        case 2:
            document.getElementById('miPanel2').classList.add("bg-gray-active");
            document.getElementById('miPanel22').classList.add("bg-gray-active");
            break;
        case 3:
            document.getElementById('miPanel3').classList.add("bg-gray-active");
            document.getElementById('miPanel33').classList.add("bg-gray-active");
            break;
        case 4:
            document.getElementById('miPanel4').classList.add("bg-gray-active");
            document.getElementById('miPanel44').classList.add("bg-gray-active");
            break;
        case 5:
            document.getElementById('miPanel5').classList.add("bg-gray-active");
            document.getElementById('miPanel55').classList.add("bg-gray-active");
            break;
        case 6:
            document.getElementById('miPanel6').classList.add("bg-gray-active");
            document.getElementById('miPanel66').classList.add("bg-gray-active");
            break;
        case 7:
            document.getElementById('miPanel7').classList.add("bg-gray-active");
            document.getElementById('miPanel77').classList.add("bg-gray-active");
            break;
        case 8:
            document.getElementById('miPanel8').classList.add("bg-gray-active");
            document.getElementById('miPanel88').classList.add("bg-gray-active");
            break;
        default:
            break;
    }


}

function colorPanelTigoCasaLimpia(valor) {

    document.getElementById('miPanel5').classList.remove("bg-gray-active");
    document.getElementById('miPanel6').classList.remove("bg-gray-active");
    document.getElementById('miPanel7').classList.remove("bg-gray-active");
    document.getElementById('miPanel8').classList.remove("bg-gray-active");
    document.getElementById('miPanel9').classList.remove("bg-gray-active");
    document.getElementById('miPanelX').classList.remove("bg-gray-active");
    document.getElementById('miPanelXI').classList.remove("bg-gray-active");
    document.getElementById('miPanelXII').classList.remove("bg-gray-active");

    document.getElementById('miPanel55').classList.remove("bg-gray-active");
    document.getElementById('miPanel66').classList.remove("bg-gray-active");
    document.getElementById('miPanel77').classList.remove("bg-gray-active");
    document.getElementById('miPanel88').classList.remove("bg-gray-active");
    document.getElementById('miPanel99').classList.remove("bg-gray-active");
    document.getElementById('miPanelXX').classList.remove("bg-gray-active");
    document.getElementById('miPanelXXI').classList.remove("bg-gray-active");
    document.getElementById('miPanelXXII').classList.remove("bg-gray-active");



    switch (valor) {
        case 5:
            document.getElementById('miPanel5').classList.add("bg-gray-active");
            document.getElementById('miPanel55').classList.add("bg-gray-active");
            break;
        case 6:
            document.getElementById('miPanel6').classList.add("bg-gray-active");
            document.getElementById('miPanel66').classList.add("bg-gray-active");
            break;
        case 7:
            document.getElementById('miPanel7').classList.add("bg-gray-active");
            document.getElementById('miPanel77').classList.add("bg-gray-active");
            break;
        case 8:
            document.getElementById('miPanel8').classList.add("bg-gray-active");
            document.getElementById('miPanel88').classList.add("bg-gray-active");
            break;
        case 9:
            document.getElementById('miPanel9').classList.add("bg-gray-active");
            document.getElementById('miPanel99').classList.add("bg-gray-active");
            break;
        case 10:
            document.getElementById('miPanelX').classList.add("bg-gray-active");
            document.getElementById('miPanelXX').classList.add("bg-gray-active");
            break;
        case 11:
            document.getElementById('miPanelXI').classList.add("bg-gray-active");
            document.getElementById('miPanelXXI').classList.add("bg-gray-active");
            break;
        case 12:
            document.getElementById('miPanelXII').classList.add("bg-gray-active");
            document.getElementById('miPanelXXII').classList.add("bg-gray-active");
            break;
        default:
            break;
    }


}

function colorPanelCotizaciones(valor) {

    document.getElementById('miPanel1').classList.remove("bg-gray-active");
    document.getElementById('miPanel2').classList.remove("bg-gray-active");
    document.getElementById('miPanel3').classList.remove("bg-gray-active");
    document.getElementById('miPanel4').classList.remove("bg-gray-active");
    document.getElementById('miPanel5').classList.remove("bg-gray-active");

    document.getElementById('miPanel11').classList.remove("bg-gray-active");
    document.getElementById('miPanel22').classList.remove("bg-gray-active");
    document.getElementById('miPanel33').classList.remove("bg-gray-active");
    document.getElementById('miPanel44').classList.remove("bg-gray-active");
    document.getElementById('miPanel55').classList.remove("bg-gray-active");

    switch (valor) {
        case 1:
            document.getElementById('miPanel1').classList.add("bg-gray-active");
            document.getElementById('miPanel11').classList.add("bg-gray-active");
            break;
        case 2:
            document.getElementById('miPanel2').classList.add("bg-gray-active");
            document.getElementById('miPanel22').classList.add("bg-gray-active");
            break;
        case 3:
            document.getElementById('miPanel3').classList.add("bg-gray-active");
            document.getElementById('miPanel33').classList.add("bg-gray-active");
            break;
        case 4:
            document.getElementById('miPanel4').classList.add("bg-gray-active");
            document.getElementById('miPanel44').classList.add("bg-gray-active");
            break;
        case 5:
            document.getElementById('miPanel5').classList.add("bg-gray-active");
            document.getElementById('miPanel55').classList.add("bg-gray-active");
            break;
        default:
            break;
    }
}

function rechazar(solicitud) {
    swal({
        title: solicitud,
        text: "¿Está seguro Rechazar esta solicitud?",
        type: "input",
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "SI",
        cancelButtonText: "NO",
        animation: "slide-from-top",
        inputPlaceholder: "Observación requerida para seguimiento."
    },
            function (inputValue) {
                if (inputValue === false)
                    return false;
                if (inputValue === "") {
                    swal.showInputError("Es requerida una observación !");
                    return false
                }

                swal({
                    title: "¡Muy bien!!",
                    text: "Solicitud : " + solicitud + " Rechazada con Éxito.",
                    type: "success"
                },
                        function (isConfirm) {
                            if (isConfirm) {
                                accionSolicitudes(3, inputValue, "");
                            }
                        }
                );
            }
    );

}

function suspender(solicitud) {
    swal({
        title: solicitud,
        text: "¿Está seguro suspender esta solicitud?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si, Suspender !",
        cancelButtonText: "No, Cancelar !",
        closeOnConfirm: false,
        closeOnCancel: false
    },
            function (isConfirm) {
                if (isConfirm) {
                    swal({
                        title: "Suspender !",
                        text: "Solicitud : " + solicitud + " Suspendida con Éxito.",
                        type: "success"
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    accionSolicitudes(2, 0, "");
                                }
                            }
                    );
                } else {
                    swal("Cancelado", "Se conserva el estado de la solicitud :)", "error");
                }
            });
}

function vobo(solicitud) {
    swal({
        title: solicitud,
        text: "¿Está seguro que esta solicitud requiere una segunda aprobación?",
        type: "input",
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "SI",
        cancelButtonText: "NO",
        animation: "slide-from-top",
        inputPlaceholder: "Observación requerida para seguimiento."
    },
            function (inputValue) {
                if (inputValue === false)
                    return false;
                if (inputValue === "") {
                    swal.showInputError("Es requerida una observación !");
                    return false
                }

                swal({
                    title: "¡Muy bien!!",
                    text: "Solicitud enviada para una segunda aprobación (VoBo).",
                    type: "success"
                },
                        function (isConfirm) {
                            if (isConfirm) {
                                accionSolicitudes(4, inputValue, "");
                            }
                        }
                );
            }
    );
}
function casalimpiaProceso(solicitud) {
    swal({
        title: solicitud,
        text: "¿Está seguro de pausar esta solicitud?",
        type: "input",
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "SI",
        cancelButtonText: "NO",
        animation: "slide-from-top",
        inputPlaceholder: "Observación requerida para seguimiento."
    },
            function (inputValue) {
                if (inputValue === false)
                    return false;

                var temIn = inputValue.trim();
                if (temIn === "") {
                    swal.showInputError("Es requerida una observación !");
                    return false
                } else {
                    if (temIn.length < 10 || temIn.length > 100) {
                        swal.showInputError("Es requerida una observación !");
                        return false
                    } else {
                        accionSolicitudes(13, 0, inputValue);
                        swal({
                            title: "¡Muy bien!!",
                            text: "Solicitud : " + solicitud + " pausada con Éxito.",
                            type: "success"
                        },
                                function (isConfirm) {
                                    if (isConfirm) {

                                    }
                                }
                        );
                    }
                }
            }
    );
}


function casalimpiaReanudar(solicitud) {
    var tipoSolicitud = "";
    swal({
        title: solicitud,
        text: "Indique el tiempo sugerido para adicionar en la ejecución de este requerimiento :",
        type: "input",
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "SI",
        cancelButtonText: "NO",
        animation: "slide-from-top",
        inputPlaceholder: "Tiempo.",
    },
            function (inputValue) {
                if (inputValue === false)
                    return false;
                if (inputValue === "") {
                    swal.showInputError("Es necesario un valor !");
                    return false
                }
                if (isNaN(inputValue)) {
                    swal.showInputError("Por favor ingrese solo valortes numericos !!!!");
                    return false
                }
                if (inputValue < 1) {
                    swal.showInputError("Por favor ingrese mas de 1 día  para esta solicitud !!!!");
                    return false
                }

                if (inputValue > 100) {
                    swal.showInputError("Por favor ingrese una catidad de valores en dias valida !!!!");
                    return false
                }

                swal({
                    title: "Esta seguro?",
                    text: "Su seleciion actual es " + tipoSolicitud + " !!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Si, Estoy seguro!!",
                    cancelButtonText: "Cancelar",
                    closeOnConfirm: false
                },
                        function () {
                            swal({
                                title: "¡Muy bien!!",
                                text: "Solicitud enviada a Casalimpia con una adicion de  " + inputValue + " días para su gestión.",
                                type: "success"
                            },
                                    function (isConfirm) {
                                        if (isConfirm) {
                                            accionSolicitudes(14, inputValue, tipoSolicitud);
                                        }
                                    }
                            );

                        });
            }
    );
}


function casalimpia(solicitud, sistema, tipoMant) {
    var tipoSolicitud = document.getElementById("tablaPrincipal:miClasificacion").value;
    var textSistem = "";
    var textTipoMant = "";
    if (tipoSolicitud === "-1") {
        swal({
            title: "¡Clasificacion!!",
            text: "Debe seleccionar el tipo de clasificacion.",
            type: "error"
        },
                function (isConfirm) {
                    if (isConfirm) {

                    }
                }
        );

    } else {
        if (parseInt(sistema + "") > 0) {
            //valido que se halla seleccionado un sistema
            var tipoSistema = document.getElementById("tablaPrincipal:miSistema").value;


            if (tipoSistema === "-1") {
                swal({
                    title: "¡Sistema!!",
                    text: "Debe seleccionar un Sistema.",
                    type: "error"
                },
                        function (isConfirm) {
                            if (isConfirm) {

                            }
                        }
                );

                return 0;
            } else {
                textSistem = tipoSistema;
            }
        }
        if (parseInt(tipoMant + "") > 0) {
            //valido que se halla seleccionado un sistema
            var tipoTipoMant = document.getElementById("tablaPrincipal:miTipoMant").value;

            if (tipoTipoMant === "-1") {
                swal({
                    title: "¡Tipo!!",
                    text: "Debe seleccionar un Tipo.",
                    type: "error"
                },
                        function (isConfirm) {
                            if (isConfirm) {

                            }
                        }
                );

                return 0;

            } else {
                textTipoMant = tipoTipoMant;
            }
        }
        swal({
            title: solicitud,
            text: "Indique el tiempo sugerido para la ejecución de este requerimiento :",
            type: "input",
            showCancelButton: true,
            closeOnConfirm: false,
            confirmButtonText: "SI",
            cancelButtonText: "NO",
            animation: "slide-from-top",
            inputPlaceholder: "Tiempo mínimo sugerido a partir de 1 Días.",
        },
                function (inputValue) {
                    if (inputValue === false)
                        return false;
                    if (inputValue === "") {
                        swal.showInputError("Es necesario un valor !");
                        return false
                    }
                    if (isNaN(inputValue)) {
                        swal.showInputError("Por favor ingrese solo valortes numericos !!!!");
                        return false
                    }

                    if (inputValue < 1) {
                        swal.showInputError("Por favor ingrese mas de 1 Días para esta solicitud !!!!");
                        return false
                    }


                    swal({
                        title: "Esta seguro?",
                        text: "Su seleccion actual es " + tipoSolicitud + " !!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si, Estoy seguro!!",
                        cancelButtonText: "Cancelar",
                        closeOnConfirm: false
                    },
                            function () {
                                swal({
                                    title: "¡Muy bien!!",
                                    text: "Solicitud enviada a Casalimpia con " + inputValue + " días para su gestión, con una clasificación = " + tipoSolicitud + ".",
                                    type: "success"
                                },
                                        function (isConfirm) {
                                            if (isConfirm) {
                                                accionSolicitudesCategorias(5, inputValue, tipoSolicitud, textSistem, textTipoMant);
                                            }
                                        }
                                );

                            });
                }
        );

    }

}

function casalimpiaCotizacion(solicitud, ans) {

    swal({
        title: "ANS Cotización N° " + solicitud,
        text: "Indique el tiempo sugerido para la ejecución de esta cotización :",
        type: "input",
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "SI",
        cancelButtonText: "NO",
        animation: "slide-from-top",
        inputPlaceholder: "Tiempo mínimo sugerido a partir de " + ans + " Días.",
    },
            function (inputValue) {
                if (inputValue === false)
                    return false;
                if (inputValue === "") {
                    swal.showInputError("Es necesario un valor !");
                    return false
                }
                if (isNaN(inputValue)) {
                    swal.showInputError("Por favor ingrese solo valortes numericos !!!!");
                    return false
                }

                if (inputValue < ans) {
                    swal.showInputError("Por favor ingrese mas de " + ans + " Días para esta solicitud !!!!");
                    return false
                }


                swal({
                    title: "Esta seguro?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Si, Estoy seguro!!",
                    cancelButtonText: "Cancelar",
                    closeOnConfirm: false
                },
                        function () {
                            swal({
                                title: "¡Muy bien!!",
                                text: "Cotización enviada a Casalimpia con " + inputValue + " días para su gestión",
                                type: "success"
                            },
                                    function (isConfirm) {
                                        if (isConfirm) {
                                            accionCotizacion(5, inputValue, "");
                                        }
                                    }
                            );

                        });
            }
    );


}
function eliminarCotizacion(pkCotizacion) {

    swal({
        title: "Cotización N° " + pkCotizacion,
        text: "¿Está seguro de eliminar la cotización?",
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "Si, Eliminar!",
        cancelButtonText: "Cancelar",
        animation: "slide-from-top",
    },
            function (isConfirm) {
                if (isConfirm) {
                    eliminarCot(pkCotizacion);
                }
            }

    );


}

function rechazarCotizacion(solicitud) {
    swal({
        title: "Cotización N° " + solicitud,
        text: "¿Está seguro Rechazar esta Cotización?",
        type: "input",
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "SI",
        cancelButtonText: "NO",
        animation: "slide-from-top",
        inputPlaceholder: "Observación requerida para seguimiento."
    },
            function (inputValue) {
                if (inputValue === false)
                    return false;
                if (inputValue === "") {
                    swal.showInputError("Es requerida una observación !");
                    return false
                }

                swal({
                    title: "¡Muy bien!!",
                    text: "La cotización fue rechazada con exito.",
                    type: "success"
                },
                        function (isConfirm) {
                            if (isConfirm) {
                                accionCotizacionRechazar(3, inputValue, "");
                            }
                        }
                );
            }
    );

}

function casalimpiaVoBo(solicitud) {
    var tipoSolicitud = "";
    swal({
        title: solicitud,
        text: "Indique el tiempo sugerido para la ejecución de este requerimiento :",
        type: "input",
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "SI",
        cancelButtonText: "NO",
        animation: "slide-from-top",
        inputPlaceholder: "Tiempo mínimo sugerido a partir de 1 Días.",
    },
            function (inputValue) {
                if (inputValue === false)
                    return false;
                if (inputValue === "") {
                    swal.showInputError("Es necesario un valor !");
                    return false
                }
                if (isNaN(inputValue)) {
                    swal.showInputError("Por favor ingrese solo valortes numericos !!!!");
                    return false
                }

                if (inputValue < 1) {
                    swal.showInputError("Por favor ingrese mas de 1 Días para esta solicitud !!!!");
                    return false
                }


                swal({
                    title: "Seleccione",
                    text: "la clasificación para esta solicitud !!!",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "ADMINISTRATIVO",
                    cancelButtonColor: "#3790BF",
                    cancelButtonText: "COMERCIAL",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                        function (isConfirm) {
                            if (isConfirm) {
                                tipoSolicitud = "ADMINISTRATIVO";
                            } else {
                                tipoSolicitud = "COMERCIAL";
                            }

                            swal({
                                title: "Esta seguro?",
                                text: "Su seleciion actual es " + tipoSolicitud + " !!",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Si, Estoy seguro!!",
                                cancelButtonText: "Cancelar",
                                closeOnConfirm: false
                            },
                                    function () {
                                        swal({
                                            title: "¡Muy bien!!",
                                            text: "Solicitud enviada a Casalimpia con " + inputValue + " días para su gestión, con una clasificación = " + tipoSolicitud + ".",
                                            type: "success"
                                        },
                                                function (isConfirm) {
                                                    if (isConfirm) {
                                                        accionSolicitudes(8, inputValue, tipoSolicitud);
                                                    }
                                                }
                                        );

                                    });
                        });
            }
    );
}

function cerrarAutomaticaCasaLimpia(solicitud) {
    swal({
        title: solicitud,
        text: "Observación requerida para cierre",
        type: "input",
        showCancelButton: false,
        closeOnConfirm: false,
        confirmButtonText: "Ingresar",
        cancelButtonText: "NO",
        animation: "slide-from-top",
        inputPlaceholder: "Observación requerida para seguimiento."
    },
            function (inputValue) {
                if (inputValue === false)
                    return false;
                if (inputValue === "") {
                    swal.showInputError("Es requerida una observación !");
                    return false
                }

                accionSolicitudes(6, inputValue, "");
                swal({
                    title: "¡Muy bien!!",
                    text: "Solicitud : " + solicitud + " cerrada con Éxito.",
                    type: "success"
                },
                        function (isConfirm) {
                            if (isConfirm) {

                            }
                        }
                );
            }
    );

}

function cerradaCasaLimpia(solicitud) {


    swal({
        title: solicitud,
        text: "¿Está seguro de cerrar esta solicitud?",
        type: "input",
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "SI",
        cancelButtonText: "NO",
        animation: "slide-from-top",
        inputPlaceholder: "Observación requerida para seguimiento."
    },
            function (inputValue) {
                if (inputValue === false)
                    return false;
                if (inputValue === "") {
                    swal.showInputError("Es requerida una observación !");
                    return false
                }

                swal({
                    title: "¡Muy bien!!",
                    text: "Solicitud : " + solicitud + " cerrada con Éxito.",
                    type: "success"
                },
                        function (isConfirm) {
                            if (isConfirm) {
                                accionSolicitudes(6, inputValue, "");
                            }
                        }
                );
            }
    );

}

function procesarDeNuevo(solicitud) {
    swal({
        title: solicitud,
        text: "¿Está seguro procesar de nuevo esta solicitud?",
        type: "input",
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "SI",
        cancelButtonText: "NO",
        animation: "slide-from-top",
        inputPlaceholder: "Observación requerida para seguimiento."
    },
            function (inputValue) {
                if (inputValue === false)
                    return false;
                if (inputValue.trim() === "") {
                    swal.showInputError("Es requerida una observación !");
                    return false
                }
                accionSolicitudes(9, inputValue.trim(), "");
                swal({
                    title: "¡Muy bien!!",
                    text: "Solicitud : " + solicitud + " enviada a procesar de nuevo",
                    type: "success"
                },
                        function (isConfirm) {

                        }
                );
            }
    );

}

function evaluacionTigo(solicitud, valor) {
    swal({
        title: "Evaluación !!!",
        text: "Valoración guardada con éxito.",
        timer: 2000,
        showConfirmButton: false
    },
            function () {
                accionSolicitudes(7, valor, "");
            }
    );

}

function cambiaClasificacion() {
    var tipoSolicitud = document.getElementById("tablaPrincipal:miClasificacionR").value;
    if (tipoSolicitud === "-1") {
        swal({
            title: "¡Clasificacion!!",
            text: "Debe seleccionar el tipo de clasificacion.",
            type: "error"
        },
                function (isConfirm) {
                    if (isConfirm) {

                    }
                }
        );

    } else {
        swal({
            title: "Esta seguro?",
            text: "Cambiar clasificacion a " + tipoSolicitud + " !!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Estoy seguro!!",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false
        },
                function () {
                    swal({
                        title: "¡Muy bien!!",
                        text: "Cambios en la clasificación " + tipoSolicitud + ".",
                        type: "success"
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    accionSolicitudes(12, "", tipoSolicitud);
                                }
                            }
                    );

                });


    }

}

function notificaiones(parte1, parte2) {

    var notification = new Notification(parte1, {
        icon: 'https://sic.casalimpia.co/Sic/images/version2.png',
        body: parte2

    });
}


