function listar(lista) {

    var idBanner = document.getElementById('machine1');
    idBanner.innerHTML = "";
    var empleados = new Array();
    var list2 = lista;
    empleados = list2.split("/,/");

    for (var i = 0; i < empleados.length; i++) {
        idBanner.innerHTML += "<div class='text-center'>" + empleados[i] + "</div>";
    }
    const btn = document.querySelector('#randomizeButton');
    const btn2 = document.querySelector('#stopButton');
    const el1 = document.querySelector('#machine1');
    const machine1 = new SlotMachine(el1, {active: 0, delay: 1000});

    btn.addEventListener('click', () => {
        machine1.shuffle(50);
    });
    btn2.addEventListener('click', () => {
        machine1.stop();
    });
}