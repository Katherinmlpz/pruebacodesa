function miFun(mes, total, color) {

    var array = mes.split(",");
    var array1 = total.split(",");
    var array2 = color.split(",");


    var myObj = {"cars": []};
    for (var i = 0; i < array.length; i++) {
        myObj.cars[i] = {"mes": array[i], "total": array1[i], "color": array2[i]};
    }
    function generateChartData() {
        var chartData = [];
        for (var i = 0; i < myObj.cars.length; i++) {

            chartData.push({
                "country": myObj.cars[i].mes,
                "visits": myObj.cars[i].total,
                "color": myObj.cars[i].color
            });
        }

        return chartData;
    }
    var chart = AmCharts.makeChart("chartdiv", {

        "type": "serial",
        "theme": "light",
        "dataProvider": generateChartData(),
        "valueAxes": [{
                "gridColor": "#FFFFFF",
                "gridAlpha": 0.2,
                "dashLength": 0
            }],
        "gridAboveGraphs": true,
        "startDuration": 2,
        "graphs": [{
                "balloonText": "[[category]]: <b>[[value]]</b>",
                "fillColorsField": "color",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "type": "column",
                "valueField": "visits"
            }],
        "chartCursor": {
            "categoryBalloonEnabled": false,
            "cursorAlpha": 0,
            "zoomable": false
        },
        "categoryField": "country",
        "categoryAxis": {
            "gridPosition": "start",
            "gridAlpha": 0,
        },
        "export": {
            "enabled": true
        },
        "responsive": {
            "enabled": true
        }

    });
}
;
